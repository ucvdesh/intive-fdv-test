import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import rootReducer from "../redux";

export default function configureStore(initialState) {
  const store = createStore(
    rootReducer,
    applyMiddleware(thunk),
    initialState,
    window.devToolsExtension ? window.devToolsExtension() : undefined
  );

  if (module.hot) {
    // Enable Webpack hot module replacement for redux
    module.hot.accept("../redux", () => {
      const nextRootReducer = require("../redux").default;
      store.replaceReducer(nextRootReducer);
    });
  }

  return store;
}
