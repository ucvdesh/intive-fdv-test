/* Importing packages */
import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import "../styles/App.css";
/* Component calls */
import InputField from "../components/InputField";
import SelectField from "../components/SelectField";
import TableArea from "../components/TableArea";
import Button from "../components/Button";
/* Reducers calls */
import {
  changeNameInput,
  changeAgeInput,
  changePositionSelect,
  thunkGetPlayers,
  getPositionState,
  getPlayersState,
  getSearchNameState,
  getSearchPositionState,
  getSearchAgeState
} from "../redux/playersRedux";

const letters = /^[A-Za-zñÑáéíóúÁÉÍÓÚ\s]*$/;
const numbers = /^[0-9]*$/;
class App extends Component {
  componentWillMount() {
    this.props.actions.thunkGetPlayers();
  };

  handleNameChange = name => {
    if (name.match(letters)) {
      this.props.actions.changeNameInput(name);
    }
  };

  handleAgeChange = age => {
    if (age.match(numbers)) {
      this.props.actions.changeAgeInput(age);
    }
  };

  handlePositionChange = position => {
    this.props.actions.changePositionSelect(position);
  };

  handleFilter = () => {
    try {
      if (this.props.searchAge > 40)
        // eslint-disable-next-line
        throw "Age cannot be greater than 40";
      else if (this.props.searchAge < 18 && this.props.searchAge !== "")
        // eslint-disable-next-line
        throw "Age cannot be lower than 18";
      else this.props.actions.thunkGetPlayers();
    } catch (e) {
      alert(e);
    }
  };

  render() {
    return (
      <div>
        <div className="container">
          <h1>Football Player Finder</h1>
          <div className="row">
            <div className="col-sm-4 col-md-3 finders">
              <InputField
                className="form-control"
                placeholder="Player Name"
                type="text"
                onInputChange={this.handleNameChange}
                value={this.props.searchName}
              />
            </div>
            <div className="col-sm-4 col-md-3 finders">
              <SelectField
                className="form-control"
                options={this.props.positions}
                changeSelect={this.handlePositionChange}
                value={this.props.searchPosition}
              />
            </div>
            <div className="col-sm-4 col-md-3 finders">
              {/*not using type='number' to avoid problems with firefox accepting other things than numbers*/}
              <InputField
                className="form-control"
                placeholder="Age"
                type="text"
                onInputChange={this.handleAgeChange}
                value={this.props.searchAge}
              />
            </div>
            <div className="col-sm-12 col-md-3 finders">
              <Button
                className="btn btn-outline-primary btn-block"
                title="Search"
                onClick={this.handleFilter}
              />
            </div>
          </div>
          <TableArea
            className="table table-striped table-hover"
            players={this.props.players}
          />
        </div>
      </div>
    );
  }
}

const mS = state => {
  return {
    players: getPlayersState(state.players),
    positions: getPositionState(state.players),
    searchName: getSearchNameState(state.players),
    searchPosition: getSearchPositionState(state.players),
    searchAge: getSearchAgeState(state.players)
  };
};

const mD = dispatch => {
  return {
    actions: bindActionCreators(
      {
        changeNameInput,
        changeAgeInput,
        changePositionSelect,
        thunkGetPlayers
      },
      dispatch
    ),
    dispatch
  };
};

export default connect(
  mS,
  mD
)(App);
