import moment from "moment";
import { createSelector } from "reselect";
/* Actions */
const CHANGE_NAME_INPUT = "CHANGE_NAME_INPUT";
const CHANGE_AGE_INPUT = "CHANGE_AGE_INPUT";
const CHANGE_POSITION_SELECT = "CHANGE_POSITION_SELECT";
const FILTER_PLAYERS = "FILTER_PLAYERS";

const initialState = {
  data: [],
  positions: [
    "Attacking Midfield",
    "Central Midfield",
    "Centre-Back",
    "Centre-Forward",
    "Centre-Forward",
    "Defensive Midfield",
    "Keeper",
    "Left Midfield",
    "Left Wing",
    "Left-Back",
    "Right-Back"
  ],
  searchName: "",
  searchAge: "",
  searchPosition: "",
  today: moment()
};
/* Reducers */
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case CHANGE_NAME_INPUT:
      return { ...state, searchName: action.payload };
    case CHANGE_AGE_INPUT:
      return { ...state, searchAge: action.payload };
    case CHANGE_POSITION_SELECT:
      return { ...state, searchPosition: action.payload };
    case FILTER_PLAYERS:
      let data = "";
      if (
        state.searchName !== "" ||
        state.searchAge !== "" ||
        state.searchPosition !== ""
      ) {
        data = action.payload.filter(player => {
          const birth = moment(player.dateOfBirth);
          const age = state.today.diff(birth, "years");
          let accept = false;
          if (state.searchAge === "") {
            accept = true;
          }
          /* validating lowercases in each cases and triming extra whitespaces */
          // eslint-disable-next-line
          return (
            player.name
              .toLowerCase()
              .includes(state.searchName.trim().toLowerCase()) &&
            player.position
              .toLowerCase()
              .includes(state.searchPosition.trim().toLowerCase()) && // eslint-disable-next-line
            (age === parseInt(state.searchAge) || accept)
          );
        });
      } else {
        data = action.payload;
      }
      return { ...state, data: data, loading: false };
    default:
      return state;
  }
}
/* Fetching all players */
const fetchPlayers = () => {
  return fetch(
    "https://football-players-b31f2.firebaseio.com/players.json?print=pretty"
  );
};
/* thunk */
export const thunkGetPlayers = () => {
  return dispatch => {
    return fetchPlayers()
      .then(response => response.json())
      .then(players => {
        try {
          if (players.error)
            // eslint-disable-next-line
            throw "Error retrieving data from the API";
          else return dispatch(filterPlayers(players));
        } catch (e) {
          alert(e);
        }
      });
  };
};
/* Action Creators */
export const filterPlayers = players => {
  return { type: FILTER_PLAYERS, payload: players };
};
export const changeNameInput = name => {
  return { type: CHANGE_NAME_INPUT, payload: name };
};
export const changeAgeInput = age => {
  return { type: CHANGE_AGE_INPUT, payload: age };
};
export const changePositionSelect = position => {
  return { type: CHANGE_POSITION_SELECT, payload: position };
};
/* Selectors */
const getPlayers = state => state.data;
const getPosition = state => state.positions;
const getSearchName = state => state.searchName;
const getSearchPosition = state => state.searchPosition;
const getSearchAge = state => state.searchAge;
/* Reselect functions */
export const getPlayersState = createSelector([getPlayers], players => players);
export const getPositionState = createSelector(
  [getPosition],
  positions => positions
);
export const getSearchNameState = createSelector(
  [getSearchName],
  SearchName => SearchName
);
export const getSearchPositionState = createSelector(
  [getSearchPosition],
  SearchPosition => SearchPosition
);
export const getSearchAgeState = createSelector(
  [getSearchAge],
  SearchAge => SearchAge
);
