import { combineReducers } from "redux";
import PlayerReducer from "./playersRedux";

const rootReducer = combineReducers({
  players: PlayerReducer
});

export default rootReducer;
