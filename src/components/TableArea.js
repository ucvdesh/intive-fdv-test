import React, { Component } from "react";
import moment from "moment";

const today = moment();

class TableArea extends Component {
  setTableRow = players => {
    return players.map((player, j) => {
      const birth = moment(player.dateOfBirth);
      const age = today.diff(birth, "years");
      return (
        <tr key={j}>
          <td>{player.name}</td>
          <td>{player.position}</td>
          <td>{player.nationality}</td>
          <td>{age}</td>
        </tr>
      );
    });
  };

  render() {
    return (
      <div style={{ textAlign: "center" }}>
        <table className={this.props.className}>
          <thead className="thead-dark">
            <tr>
              <th scope="col">Player</th>
              <th scope="col">Position</th>
              <th scope="col">Team</th>
              <th scope="col">Age</th>
            </tr>
          </thead>
          <tbody>{this.setTableRow(this.props.players)}</tbody>
        </table>
      </div>
    );
  }
}

export default TableArea;
