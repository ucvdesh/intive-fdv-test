import React from "react";

const InputField = props => (
  <input
    type={props.type}
    className={props.className}
    placeholder={props.placeholder}
    onChange={event => props.onInputChange(event.target.value)}
    value={props.value}
  />
);

export default InputField;
