import React from "react";

const SelectField = props => {
  const setOptions = props.options.map((option, i) => {
    return (
      <option key={i} value={option}>
        {option}
      </option>
    );
  });

  return (
    <select
      className={props.className}
      value={props.value}
      onChange={event => props.changeSelect(event.target.value)}
    >
      <option value="">Position</option>
      {setOptions}
    </select>
  );
};

export default SelectField;
