import React from "react";

const InputField = props => {
  return (
    <button className={props.className} onClick={props.onClick}>
      {props.title}
    </button>
  );
};

export default InputField;
